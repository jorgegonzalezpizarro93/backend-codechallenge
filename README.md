# Backend-codechallenge

Desarrollo de aplicación php utilizando Laravel como framework y Mysql como sistema de persistencia.
La aplicación implementa los siguientes requisitos :


>Imaginemos que un cliente solicita el envío de un pedido mediante una llamada a la API REST para almacenarlo en la base de datos.
> El pedido debe contener:

- **Nombre y apellidos del cliente**

- **Email (Único por cliente)**

- **Teléfono**

- **Dirección de entrega (solo puede existir una por pedido)**

- **Fecha de entrega**

- **Franja de hora seleccionada para la entrega (variable, pueden ser desde franjas de 1h hasta de 8h)**


>Una vez tenemos guardada la información del pedido, debe asignarse a un driver que tengamos dado de alta en el sistema de forma aleatoria.

>Por otro lado, nuestros drivers mediante su aplicación, necesitan obtener el listado de tareas para completar en el día. Es necesario contar con un endpoint que reciba como parámetro el ID del driver y la fecha de los pedidos que queremos obtener y nos devuelva un JSON con el listado.

***
# Instalación


```sh
git clone https://gitlab.com/jorgegonzalezpizarro93/backend-codechallenge.git
cd backend-codechallenge 
composer install || update 
```


### Docker
Se proporciona un archivo Dockerfile para construir una imagen Php con el entorno necesario de la aplicación , tambien una imagen Mysql .
El archivo docker-compose.yml generará ambas imágenes. 

```sh
cd backend-codechallenge 
docker-compose up -d --build //genera ambas imágenes
docker run -d  -p 8080:80 -v /$(pwd):/var/www $nombre_imagen 
```



```sh
localhost:8080 || 127.0.0.1:8080
```

### Migraciones 

La configuracion de la base de datos viene dada en el fichero .env no versionable , las variables de entorno son pasadas a la imagen de docker mysql.
- DB_CONNECTION=mysql
- DB_HOST=127.0.0.1
- DB_PORT=3306
- DB_DATABASE=orders
- DB_USERNAME=root
- DB_PASSWORD=root

Para ejecutar las migraciones :
```sh
cd backend-codechallenge 
docker exec -it $_IMAGE_NAME bash php artisan migrate //DOCKER

php artisan migrate //ENTORNO LOCAL
```

        
***

## Arquitectura
Se ha adoptado una Arquitectura hexagonal (puerto->adaptador) bajo una capa de programacion orientada a objetos pretendiendo conseguir cierto grado de abstraccion.
Se han implementado las buenas practicas de DDD para separar conceptos e implementaciones , la aplicacion (a nivel estructural) queda separada en contextos de la siguiente forma :

* Punto de entrada - /API : 
-- Contiene los puntos de acceso a la aplicación Controllers & EventSubscribers
* Capa de aplicación - /Application
-- Contiene los casos de uso de la aplicación , se ha aplicado el patron Command/Query -> Handler separando las solicitudes de modificacion del sistema de las de lectura y encapsulando la petición en objetos Command/Query. 
* Capa de dominio - /Domain
-- Contiene los modelos de Dominio y las reglas de validacion implementadas en Value objects.
* Capa de infraestructura - /Infrastructure
*-- Contiene las implementaciones de de las interfaces del dominio .


***

## Implementación

La aplicación está dividida en 3 contextos desacoplados con la intención de que cada contexto se desarrolle y evolucione de forma autonoma al resto.

* Orders 
* Customers 
* Drivers 
    -- Agregado que contiene Drivers & Dayli Tasks

En cada uno se ha implementado la misma arquitectura . La cominicación entre contextos se realiza
en forma de eventos aplicando  el patrón pub-sub o reactividad , donde una acción "CreateOrder" desencadena el evento "OrderWasCreated" que será recogido por los contextos que lo necesiten.

La implementación en éste caso es totalmente síncrona , aun así , esta preparado para implementar colas de mensajeria en caso de optimizacion de performance.

***
### Puntos de Acceso 

-  ##### 1. Create order  


        POST /orders : 
        {
            "name" : "jorge",
            "email": "jorge.j.gonzalez.93@gmail.com",
            "lastname":"gonzalez",
            "phoneNumber": 617379364,
            "orderAddress":"c/ Fernando Poo 11 6ºB",
            "orderDate" : "11-01-12",
            "orderHourSlot":
                ["16:00","17:00"]
        }
 
-  ##### 2. Gert Daily Tasks order



        GET /dailytasks?driverId=$_driver_id&orderDate=$_orderdate : 
        -Query parameters 
        {    
            $driver_id=uuid driver;
            $order_date=fecha seleccionada
        }
 
