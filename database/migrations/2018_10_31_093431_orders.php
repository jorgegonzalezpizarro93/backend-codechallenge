<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Orders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->uuid('order_id');
            $table->primary('order_id');
            $table->uuid('driver_id')->nullable();
            $table->string('customer_name');
            $table->string('customer_last_name');
            $table->integer('phone_number');

            $table->string('email');
            $table->string('order_address');
            $table->string('order_date');
            $table->string('order_hour_slot');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
