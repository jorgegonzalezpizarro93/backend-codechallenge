<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Drivers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('drivers', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');

        });


        for ($i = 0; $i < 100; $i++) {
            \Illuminate\Support\Facades\DB::table('drivers')->insert(
                array(
                    'id' => \App\Infrastructure\Uuid::generateUuid()

                )
            );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('drivers');
    }
}
