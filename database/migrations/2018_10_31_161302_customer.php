<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Customer extends Migration
{
    public function up()
    {
        Schema::create('customer', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->string('email');

        });

        \Illuminate\Support\Facades\DB::table('customer')->insert(
            array(
                'id' => \App\Infrastructure\Uuid::generateUuid(),
                'email' => 'jorge.j.gonzalez.93@gmail.com'

            )
        );

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer');
    }
}
