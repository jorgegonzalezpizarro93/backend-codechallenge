<?php
/**
 * Created by PhpStorm.
 * User: Jorge Gonzalez
 * Date: 31/10/18
 * Time: 13:44
 */

namespace App;

interface EventDispatcher
{
    public static function dispatch(DomainEvent $domainEvent);
}
