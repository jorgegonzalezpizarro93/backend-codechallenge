<?php

namespace App\Providers;

use App\Customer\Api\CustomerEventSubscriber\OnOrderWasCreatedCheckCustomerEmailSubscriber;
use App\Customer\Domain\Events\CustomerNotFoundEvent;
use App\Driver\Domain\DailyTask\Event\OrderWasAssigned;
use App\Order\Api\DeleteOrderOnCustomerNotFound;
use App\Order\Domain\Event\OrderWasCreated;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     * @var array
     */
    protected $listen = [

        CustomerNotFoundEvent::class => [
            DeleteOrderOnCustomerNotFound::class,
        ],
        OrderWasCreated::class => [
            OnOrderWasCreatedCheckCustomerEmailSubscriber::class,
            \App\Driver\Api\DriverEventSubscriber::class,
        ],
        OrderWasAssigned::class => [
            \App\Order\Api\OrderWasAssignedSubscriber::class,
        ],

    ];

    /**
     * Register any events for your application.
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
