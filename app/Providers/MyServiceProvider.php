<?php

namespace App\Providers;

use App\Customer\Api\CustomerEventSubscriber\OnOrderWasCreatedCheckCustomerEmailSubscriber;
use App\Customer\Application\GetCustomerByEmailUseCase;
use App\Customer\Domain\CustomerRepository;
use App\Customer\Infrastructure\Model\CustomerRepositoryImplementation;
use App\DomainEventDispatcher;
use App\Driver\Api\DriverEventSubscriber;
use App\Driver\Domain\DailyTask\DailyTaskRepository;
use App\Driver\Domain\Driver\DriverRepository;
use App\Driver\Infrastructure\Repository\DailyTaskRepositoryImplementation;
use App\Driver\Infrastructure\Repository\DriverRepositoryImplementation;
use App\Driver\UseCase\AssignToDriverOnOrderWasCreated;
use App\Infrastructure\Uuid;
use App\Infrastructure\UUidGenerator;
use App\Order\Domain\OrderRepository;
use App\Order\Domain\OrderRepositoryImplementation;
use App\Order\Infrastructure\Repository;
use Illuminate\Support\ServiceProvider;

class MyServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     * @return void
     */
    public function register()
    {

        $this->app->bind(
            \App\Http\Controllers\CreateOrderController::class, function ($app) {
            return new \App\Http\Controllers\CreateOrderController(
                $this->app->make(\App\Order\Application\CreateOrder\CreateOrderUseCase::class)
            );
        }
        );

        $this->app->bind(
            \App\Order\Application\CreateOrder\CreateOrderUseCase::class, function ($app) {
            return new \App\Order\Application\CreateOrder\CreateOrderUseCase(
                $this->app->make(OrderRepository::class),
                $this->app->make(UUidGenerator::class), $this->app->make(\App\EventDispatcher::class)
            );
        }
        );
        $this->app->bind(\App\EventDispatcher::class, DomainEventDispatcher::class);
        $this->app->bind(
            \App\Order\Domain\OrderRepository::class,
            \App\Order\Infrastructure\Model\OrderRepositoryImplementation::class
        );
        $this->app->bind(CustomerRepository::class, CustomerRepositoryImplementation::class);

        $this->app->bind(\App\EventDispatcher::class, DomainEventDispatcher::class);
        $this->app->bind(
            DriverEventSubscriber::class, function ($app) {
            return new DriverEventSubscriber($app->make(AssignToDriverOnOrderWasCreated::class));
        }
        );
        $this->app->bind(
            OnOrderWasCreatedCheckCustomerEmailSubscriber::class, function ($app) {
            return new OnOrderWasCreatedCheckCustomerEmailSubscriber($app->make(GetCustomerByEmailUseCase::class));
        }
        );
        $this->app->bind(DriverRepository::class, DriverRepositoryImplementation::class);
        $this->app->bind(DailyTaskRepository::class, DailyTaskRepositoryImplementation::class);

        $this->app->bind(UUidGenerator::class, Uuid::class);
    }
}
