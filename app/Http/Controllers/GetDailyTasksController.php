<?php
/**
 * Created by PhpStorm.
 * User: jorgegonzalez
 * Date: 2/11/18
 * Time: 10:22
 */

namespace App\Http\Controllers;

use App\Driver\UseCase\Query\DailyTasksByDriverQuery;
use App\Driver\UseCase\Query\DailyTasksByDriverUseCase;
use App\Infrastructure\ListResponse;
use App\Infrastructure\ResponseSerializer;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class GetDailyTasksController
{
    public function __construct(
        DailyTasksByDriverUseCase $dailyTasksByDriverUseCase,
        ResponseSerializer $responseSerializer
    )
    {
        $this->dailyTasksByDriverUseCase = $dailyTasksByDriverUseCase;
        $this->responseSerializer = $responseSerializer;

    }

    public function __invoke(Request $request)
    {
        $response = $this->dailyTasksByDriverUseCase->query(
            new DailyTasksByDriverQuery(
                $request->get('driverId'),
                $request->get('date')

            )
        );
        if ($response->getIterator()->count() > 0) {
            return new ListResponse($response, 200, array());

        }

        return new Response("Not found ", 404, array());

    }
}
