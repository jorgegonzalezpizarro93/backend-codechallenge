<?php
/**
 * Created by PhpStorm.
 * User: jorgegonzalez
 * Date: 31/10/18
 * Time: 10:52
 */

namespace App\Http\Controllers;

use App\Order\Application\CreateOrderCommand;
use App\Order\Application\CreateOrderUseCase;
use Illuminate\Http\Request;

class CreateOrderController
{
    public function __construct(\App\Order\Application\CreateOrder\CreateOrderCommand $createOrderUseCase)
    {
        $this->createOrderUseCase = $createOrderUseCase;

    }

    public function __invoke(Request $request)
    {
        $this->createOrderUseCase->dispatch(
            new \App\Order\Application\CreateOrder\CreateOrderUseCase(
                $request->get('nombre'),
                $request->get('apellido'),
                $request->get('email'),
                $request->get('direccion_entrega'),
                $request->get('fecha_entrega'),
                $request->get('franja_horaria')

            )
        );

//        return response("",200,array());
    }
}
