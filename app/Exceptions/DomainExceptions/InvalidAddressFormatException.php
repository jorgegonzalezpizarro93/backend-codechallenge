<?php
/**
 * Created by PhpStorm.
 * User: jorgegonzalez
 * Date: 2/11/18
 * Time: 14:52
 */

namespace App\Exceptions\DomainExceptions;

use App\DomainException;

class InvalidAddressFormatException extends DomainException
{
}
