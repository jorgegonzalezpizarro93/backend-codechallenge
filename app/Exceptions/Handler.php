<?php

namespace App\Exceptions;

use App\Customer\Domain\Events\CustomerNotFoundEvent;
use App\DomainBoundedException;
use App\DomainEventDispatcher;
use App\DomainException;
use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Ramsey\Uuid\Uuid;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     * @var array
     */
    protected $dontReport = [
        //
    ];
    /**
     * A list of the inputs that are never flashed for validation exceptions.
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     * @param  \Exception $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     * @param  \Illuminate\Http\Request $request
     * @param  \Exception $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {

        if ($exception instanceof DomainException) {

            return response($exception->getMessage(), $exception->getHttpCode(), array());

        }
        if ($exception instanceof DomainBoundedException) {

            DomainEventDispatcher::dispatch(
                new CustomerNotFoundEvent(
                    Uuid::uuid1(),
                    array(
                        "order_id" => $exception->getOrderId(),
                        "customer_email" => $exception->getCustomerEmail(),
                    ),
                    new \DateTime('now')
                )
            );

            return response($exception->getMessage(), $exception->getHttpCode(), array());

        }

        return parent::render($request, $exception);
    }
}
