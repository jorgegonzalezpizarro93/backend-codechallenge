<?php
/**
 * Created by PhpStorm.
 * User: jorgegonzalez
 * Date: 2/11/18
 * Time: 12:52
 */

namespace App\Infrastructure;

use Symfony\Component\HttpFoundation\Response;

class ListResponse extends Response
{
    public function __construct(DomainList $content, int $status = 200, array $headers = array())
    {
        $this->content = $this->setListContent($content);
        parent::__construct(json_encode($this->content, true), $status, $headers);
    }

    public function setListContent(DomainList $content)
    {

        return array("List of dayly tasks " => $content);

    }
}
