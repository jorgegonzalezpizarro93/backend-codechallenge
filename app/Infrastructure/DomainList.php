<?php
/**
 * Created by PhpStorm.
 * User: jorgegonzalez
 * Date: 2/11/18
 * Time: 10:45
 */

namespace App\Infrastructure;

use Illuminate\Support\Collection;

class DomainList extends Collection
{
    public function __construct($items = [])
    {
        parent::__construct($items);
    }

    public function isNotEmpty()
    {
        foreach ($this->items as $item) {
            if (empty($item)) {
                return false;
            }
        }

        return false;
    }
}
