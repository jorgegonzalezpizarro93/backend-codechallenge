<?php
/**
 * Created by PhpStorm.
 * User: jorgegonzalez
 * Date: 31/10/18
 * Time: 9:12
 */

namespace App\Infrastructure;

interface Repository
{
    public function findById(string $id);

    public function save(Entity $model);

    public function update(Entity $model);
}
