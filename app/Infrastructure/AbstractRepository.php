<?php
/**
 * Created by PhpStorm.
 * User: jorgegonzalez
 * Date: 31/10/18
 * Time: 9:08
 */

namespace App\Infrastructure;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

/**
 * Class AbstractRepository
 * @package App\Order\Infrastructure
 */
abstract class AbstractRepository implements Repository
{
    protected $model;
    protected $app;

    public function __construct(App $app)
    {
        $this->app = $app::getFacadeApplication();
        $this->model = $this->toModel()->newQuery();
    }

    public function toModel(): Model
    {
        return $this->app->make($this->modelClass());
    }

    protected abstract function modelClass();

    public function findById(string $id): Entity
    {
        $model = $this->model->find($id);

        return $this->toEntity($model);
    }

    protected abstract function toEntity(Model $model);

    public function save(Entity $model)
    {

        $eloquetModel = $this->toModel()->fill($model->toModel());
        $eloquetModel->save();

    }

    public function update(Entity $model)
    {
        $modelToUpdate = $this->model->find($this->toModel()->fill($model->toModel())->getKey());
        $modelToUpdate->fill($model->toModel())->update();
    }
}
