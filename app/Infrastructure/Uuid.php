<?php
/**
 * Created by PhpStorm.
 * User: jorgegonzalez
 * Date: 31/10/18
 * Time: 11:04
 */

namespace App\Infrastructure;

class Uuid implements UUidGenerator
{
    public static function generateUuid()
    {
        return \Ramsey\Uuid\Uuid::uuid4()->toString();
    }

    public static function generateUuidFromString(string $id)
    {
        return \Ramsey\Uuid\Uuid::fromString($id);
    }

    public function toString()
    {
        return parent::toString();
    }
}
