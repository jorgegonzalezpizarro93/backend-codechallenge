<?php
/**
 * Created by PhpStorm.
 * User: jorgegonzalez
 * Date: 31/10/18
 * Time: 11:26
 */

namespace App\Infrastructure;

use App\Order\Infrastructure\Uuid;

abstract class Entity
{
    protected $domainEvents = array();

    public abstract function toModel();

    public function getDomainEvents()
    {
        return $this->domainEvents;
    }

    protected function generateUuid()
    {

        return \App\Infrastructure\Uuid::generateUuid();

    }
}
