<?php
/**
 * Created by PhpStorm.
 * User: jorgegonzalez
 * Date: 31/10/18
 * Time: 11:07
 */

namespace App\Infrastructure;

interface UUidGenerator
{
    public static function generateUuid();

    public static function generateUuidFromString(string $id);

    public function toString();
}
