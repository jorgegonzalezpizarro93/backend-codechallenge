<?php
/**
 * Created by PhpStorm.
 * User: jorgegonzalez
 * Date: 31/10/18
 * Time: 16:18
 */

namespace App\Driver\Infrastructure\Repository;

use App\Driver\Domain\DailyTask\DailyTaskRepository;
use App\Infrastructure\AbstractRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class DailyTaskRepositoryImplementation extends AbstractRepository implements DailyTaskRepository
{
    private const ENTITY = 'App\Driver\Domain\DailyTask\DailyTask';
    const MODEL = '\DailyTaskModel';

    public function __construct(App $app)
    {
        parent::__construct($app);
    }

    public function findByDriverIdAndDateOrder(string $driverId, string $date)
    {
        $models = $this->model->newQuery()->where('driver_id', '=', $driverId)->where('order_date', '=', $date)->get();
//        $entities=  array();
//        foreach ($models->getIterator() as $model){
//            $entities[]=$model;
//        }
        return $models;
    }

    protected function modelClass()
    {

        return 'App\Driver\Infrastructure\Model'.self::MODEL;
    }

    protected function toEntity(Model $model)
    {
        $entity = self::ENTITY;

        return $entity::create(
            $model->getKey(),
            $model->getAttribute('driver_id'),
            $model->getAttribute('driver_id'),
            $model->getAttribute('order_hour_slot'),
            $model->getAttribute('order_address'),
            $model->getAttribute('order_date')

        );

    }
}
