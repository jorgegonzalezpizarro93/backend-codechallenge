<?php
/**
 * Created by PhpStorm.
 * User: jorgegonzalez
 * Date: 31/10/18
 * Time: 16:12
 */

namespace App\Driver\Infrastructure\Repository;

use App\Driver\Domain\Driver\DriverRepository;
use App\Infrastructure\AbstractRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class DriverRepositoryImplementation extends AbstractRepository implements DriverRepository
{
    const MODEL = '\DriverModel';
    private const ENTITY = 'App\Driver\Domain\Driver\Driver';

    public function __construct(App $app)
    {
        parent::__construct($app);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|null|object
     */
    public function findRandomDriver()
    {

        return $this->model->newQuery()->inRandomOrder()->first();
    }

    protected function modelClass()
    {

        return 'App\Driver\Infrastructure\Model'.self::MODEL;
    }

    protected function toEntity(Model $model)
    {
        function toEntity(Model $model)
        {
            $entity = self::ENTITY;

            return $entity::createOrder(
                $model->getKey()
            );

        }
    }
}

