<?php
/**
 * Created by PhpStorm.
 * User: jorgegonzalez
 * Date: 31/10/18
 * Time: 16:10
 */

namespace App\Driver\Infrastructure\Model;

use Illuminate\Database\Eloquent\Model;

class DailyTaskModel extends Model
{
    public $timestamps = false;
    protected $table = 'daily_tasks';
    protected $primaryKey = 'id';
    protected $casts = [
        'id' => 'string',
    ];
    protected $fillable = [
        "id",
        "driver_id",
        "order_hour_slot",
        "order_address",
        'order_id',
        'order_date',
    ];
}
