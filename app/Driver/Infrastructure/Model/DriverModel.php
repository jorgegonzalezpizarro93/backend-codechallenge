<?php
/**
 * Created by PhpStorm.
 * User: jorgegonzalez
 * Date: 31/10/18
 * Time: 16:10
 */

namespace App\Driver\Infrastructure\Model;

use Illuminate\Database\Eloquent\Model;

class DriverModel extends Model
{
    public $timestamps = false;
    protected $casts = [
        'id' => 'string',
    ];
    protected $table = 'drivers';
    protected $fillable = [
        "id",
    ];
}
