<?php
/**
 * Created by PhpStorm.
 * User: jorgegonzalez
 * Date: 31/10/18
 * Time: 15:20
 */

namespace App\Driver\Api;

use App\Driver\UseCase\AssignToDriverOnOrderWasCreated;
use App\Driver\UseCase\AssignToDriverOnOrderWasCreatedCommand;
use Illuminate\Support\Facades\App;

class DriverEventSubscriber
{
    private $assignToDriverOnOrderWasCreatedUseCase;

    public function __construct()
    {
        $this->assignToDriverOnOrderWasCreatedUseCase = App::getFacadeApplication()->make(
            AssignToDriverOnOrderWasCreated::class
        );
    }

    public function handle($event)
    {
        $eventInformation = $event->getEvent();
        $this->assignToDriverOnOrderWasCreatedUseCase->dispatch(
            new AssignToDriverOnOrderWasCreatedCommand(
                $eventInformation['order_id'],
                $eventInformation['order_address'],
                $eventInformation['order_date'],
                $eventInformation['order_hour_slot']

            )
        );

    }
}
