<?php
/**
 * Created by PhpStorm.
 * User: jorgegonzalez
 * Date: 31/10/18
 * Time: 15:32
 */

namespace App\Driver\UseCase;

class AssignToDriverOnOrderWasCreatedCommand
{
    private $orderId;
    private $orderAddress;
    private $orderDate;
    private $orderHourSlot;

    public function __construct($orderId, $orderAddress, $orderDate, $orderHourSlot)
    {

        $this->orderId = $orderId;
        $this->orderAddress = $orderAddress;
        $this->orderDate = $orderDate;
        $this->orderHourSlot = $orderHourSlot;
    }

    /**
     * @return mixed
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @return mixed
     */
    public function getOrderAddress()
    {
        return $this->orderAddress;
    }

    /**
     * @return mixed
     */
    public function getOrderDate()
    {
        return $this->orderDate;
    }

    /**
     * @return mixed
     */
    public function getOrderHourSlot()
    {
        return $this->orderHourSlot;
    }
}
