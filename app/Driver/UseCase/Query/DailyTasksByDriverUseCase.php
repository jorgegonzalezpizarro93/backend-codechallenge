<?php
/**
 * Created by PhpStorm.
 * User: jorgegonzalez
 * Date: 2/11/18
 * Time: 10:26
 */

namespace App\Driver\UseCase\Query;

use App\Driver\Domain\DailyTask\DailyTaskRepository;
use App\Driver\Domain\DailyTask\DomainList;
use App\Driver\Domain\DailyTask\ValueObject\DailyTasksList;

class DailyTasksByDriverUseCase
{
    public function __construct(DailyTaskRepository $dailyTaskRepository)
    {
        $this->dailyTaskRepository = $dailyTaskRepository;
    }

    public function query(DailyTasksByDriverQuery $dailyTasksByDriverQuery)
    {
        return new DailyTasksList(
            $this->dailyTaskRepository->findByDriverIdAndDateOrder(
                $dailyTasksByDriverQuery->getDriverId(),
                $dailyTasksByDriverQuery->getDate()
            )
        );

    }
}
