<?php
/**
 * Created by PhpStorm.
 * User: jorgegonzalez
 * Date: 2/11/18
 * Time: 10:25
 */

namespace App\Driver\UseCase\Query;

class DailyTasksByDriverQuery
{
    private $driverId;
    private $date;

    public function __construct($driverId, $date)
    {
        $this->driverId = $driverId;
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @return mixed
     */
    public function getDriverId()
    {
        return $this->driverId;
    }
}
