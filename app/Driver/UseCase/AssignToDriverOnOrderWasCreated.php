<?php
/**
 * Created by PhpStorm.
 * User: jorgegonzalez
 * Date: 31/10/18
 * Time: 15:32
 */

namespace App\Driver\UseCase;

use App\Driver\Domain\DailyTask\DailyTask;
use App\Driver\Domain\DailyTask\DailyTaskRepository;
use App\Driver\Domain\Driver\DriverRepository;
use App\Infrastructure\UUidGenerator;
use App\Order\Domain\EventDispatcher;

class AssignToDriverOnOrderWasCreated
{
    public function __construct(
        DriverRepository $driverRepository,
        DailyTaskRepository $dailyTaskRepository,
        \App\EventDispatcher $eventDispatcher,
        UUidGenerator $UUidGenerator
    )
    {

        $this->driverRepository = $driverRepository;
        $this->daylyRepository = $dailyTaskRepository;
        $this->eventDispatcher = $eventDispatcher;
        $this->uuidGenerator = $UUidGenerator;
    }

    public function dispatch(AssignToDriverOnOrderWasCreatedCommand $assignToDriverOnOrderWasCreatedCommand)
    {

        $id = $this->uuidGenerator::generateUuid();

        $idDriver = $this->driverRepository->findRandomDriver()->getKey();

        $dailyTask = DailyTask::create(
            $id,
            $idDriver,
            $assignToDriverOnOrderWasCreatedCommand->getOrderDate(),
            $assignToDriverOnOrderWasCreatedCommand->getOrderAddress(),
            $assignToDriverOnOrderWasCreatedCommand->getOrderHourSlot(),
            $assignToDriverOnOrderWasCreatedCommand->getOrderId()
        );
        $this->daylyRepository->save($dailyTask);

        foreach ($dailyTask->getDomainEvents() as $event) {

            $this->eventDispatcher::dispatch($event);

        }

    }
}
