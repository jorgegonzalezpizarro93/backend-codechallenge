<?php
/**
 * Created by PhpStorm.
 * User: jorgegonzalez
 * Date: 31/10/18
 * Time: 16:15
 */

namespace App\Driver\Domain\Driver;

use App\Driver\Infrastructure\Model\DriverModel;
use App\Infrastructure\Repository;

interface DriverRepository extends Repository
{
    /**
     * @return DriverModel
     */
    public function findRandomDriver();
}
