<?php
/**
 * Created by PhpStorm.
 * User: jorgegonzalez
 * Date: 31/10/18
 * Time: 15:32
 */

namespace App\Driver\Domain\Driver;

use App\Infrastructure\Entity;

class Driver extends Entity
{
    public function __construct($id)
    {
        $this->id = $id;
    }

    public function toModel()
    {
        return [
            $id = $this->id,
        ];
    }
}
