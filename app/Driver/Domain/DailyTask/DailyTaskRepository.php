<?php
/**
 * Created by PhpStorm.
 * User: jorgegonzalez
 * Date: 31/10/18
 * Time: 16:18
 */

namespace App\Driver\Domain\DailyTask;

use App\Infrastructure\Repository;

interface DailyTaskRepository extends Repository
{
    public function findByDriverIdAndDateOrder(string $driverId, string $date);
}
