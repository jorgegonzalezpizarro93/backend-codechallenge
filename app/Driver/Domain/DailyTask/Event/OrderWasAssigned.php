<?php
/**
 * Created by PhpStorm.
 * User: jorgegonzalez
 * Date: 31/10/18
 * Time: 15:42
 */

namespace App\Driver\Domain\DailyTask\Event;

use App\DomainEvent;

class OrderWasAssigned extends DomainEvent
{
    public function __construct($id, array $event)
    {
        parent::__construct($id, $event, $this->createdAt());
    }

    protected function createdAt()
    {
        return new \DateTime('now');
    }
}
