<?php
/**
 * Created by PhpStorm.
 * User: Jorge Gonzalez
 * Date: 03/11/2018
 * Time: 18:08
 */

namespace App\Driver\Domain\DailyTask\ValueObject;

use App\Driver\Domain\DailyTask\DomainList;
use App\Order\Infrastructure\Entity;
use Illuminate\Support\Collection;

class DailyTasksList extends \App\Infrastructure\DomainList
{
    public function __construct(Collection $entities)
    {
        $this->items = $entities->items;
        parent::__construct($entities->items);
    }
}
