<?php
/**
 * Created by PhpStorm.
 * User: jorgegonzalez
 * Date: 31/10/18
 * Time: 15:36
 */

namespace App\Driver\Domain\DailyTask;

use App\Driver\Domain\DailyTask\Event\OrderWasAssigned;
use App\Infrastructure\Entity;

class DailyTask extends Entity
{
    private $id;
    private $driverId;
    private $orderDate;
    private $orderAddress;
    private $orderHourSlot;
    private $orderId;

    public function __construct($id, $driverId, $orderDate, $orderAddress, $orderHourSlot, $orderId)
    {
        $this->id = $id;
        $this->driverId = $driverId;
        $this->orderDate = $orderDate;
        $this->orderAddress = $orderAddress;
        $this->orderHourSlot = $orderHourSlot;
        $this->orderId = $orderId;

        $this->domainEvents["orderWasAssigned"] = new OrderWasAssigned(
            $this->generateUuid(),
            $this->toModel()
        );
    }

    public function toModel()
    {

        return [
            "id" => $this->id,
            "driver_id" => $this->driverId,
            "order_hour_slot" => $this->orderHourSlot,
            "order_address" => $this->orderAddress,
            'order_id' => $this->orderId,
            'order_date' => $this->orderDate,
        ];
    }

    public static function create($id, $driverId, $orderDate, $orderAddress, $orderHourSlot, $orderId)
    {

        return new self(
            $id,
            $driverId,
            $orderDate,
            $orderAddress,
            $orderHourSlot,
            $orderId
        );
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getDriverId()
    {
        return $this->driverId;
    }

    /**
     * @return mixed
     */
    public function getOrderDate()
    {
        return $this->orderDate;
    }

    /**
     * @return mixed
     */
    public function getOrderAddress()
    {
        return $this->orderAddress;
    }

    /**
     * @return mixed
     */
    public function getOrderHourSlot()
    {
        return $this->orderHourSlot;
    }

    /**
     * @return mixed
     */
    public function getOrderId()
    {
        return $this->orderId;
    }
}
