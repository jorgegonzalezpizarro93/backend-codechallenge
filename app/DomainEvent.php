<?php
/**
 * Created by PhpStorm.
 * User: Jorge Gonzalez
 * Date: 31/10/18
 * Time: 13:51
 */

namespace App;

abstract class DomainEvent
{
    private $id;
    private $event;
    private $dateCreated;

    public function __construct($id, array $event, $dateCreated)
    {
        $this->id = $id;
        $this->event = $event;
        $this->dateCreated = $dateCreated;
    }

    public function getAlias()
    {
        return get_class($this);
    }

    public function getEvent()
    {
        return $this->event;
    }

    abstract protected function createdAt();
}
