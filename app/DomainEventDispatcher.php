<?php
/**
 * Created by PhpStorm.
 * User: Jorge Gonzalez
 * Date: 31/10/18
 * Time: 14:13
 */

namespace App;

class DomainEventDispatcher implements EventDispatcher
{
    public static function dispatch(DomainEvent $domainEvent)
    {
        \event($domainEvent);
    }
}
