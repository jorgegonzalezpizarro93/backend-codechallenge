<?php
/**
 * Created by PhpStorm.
 * User: JorgePc
 * Date: 02/11/2018
 * Time: 19:27
 */

namespace App\Customer\Domain;

use App\DomainBoundedException;
use Throwable;

class CustomerNotFoundException extends DomainBoundedException
{
    public function __construct(
        string $message = "",
        $customerEmail,
        $orderId,
        int $code = 0,
        Throwable $previous = null
    )
    {
        $this->customerEmail = $customerEmail;
        $this->orderId = $orderId;
        $this->httpCode = 404;
        parent::__construct($message, $code, $previous, $this->httpCode);
    }

    public function getCustomerEmail()
    {
        return $this->customerEmail;
    }

    public function getOrderId()
    {
        return $this->orderId;
    }
}
