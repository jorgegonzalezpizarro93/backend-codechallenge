<?php
/**
 * Created by PhpStorm.
 * User: jorgegonzalez
 * Date: 2/11/18
 * Time: 8:17
 */

namespace App\Customer\Domain;

interface CustomerRepository
{
    public function findByEmail(string $email);
}
