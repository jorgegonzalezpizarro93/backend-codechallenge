<?php
/**
 * Created by PhpStorm.
 * User: jorgegonzalez
 * Date: 2/11/18
 * Time: 8:17
 */

namespace App\Customer\Domain;

use App\Infrastructure\Entity;

class Customer extends Entity
{
    private $id;
    private $email;

    public function __construct($id, $email)
    {
        $this->id = $id;
        $this->email = $email;
    }

    public static function create($id, $email)
    {

        return new self($id, $email);
    }

    public function toModel()
    {
        return [
            "id" => $this->id,
            "email" => $this->email,
        ];
    }
}
