<?php
/**
 * Created by PhpStorm.
 * User: jorgegonzalez
 * Date: 2/11/18
 * Time: 9:15
 */

namespace App\Customer\Domain\Events;

use App\DomainEvent;

class CustomerNotFoundEvent extends DomainEvent
{
    public function __construct($id, array $event, $dateCreated)
    {
        parent::__construct($id, $event, $this->createdAt());
    }

    protected function createdAt()
    {
        return new \DateTime('now');
    }
}
