<?php
/**
 * Created by PhpStorm.
 * User: jorgegonzalez
 * Date: 2/11/18
 * Time: 8:18
 */

namespace App\Customer\Infrastructure\Model;

use Illuminate\Database\Eloquent\Model;

class CustomerModel extends Model
{
    public $timestamps = false;
    protected $table = 'customer';
    protected $primaryKey = 'id';
    protected $casts = [
        'id' => 'string',
    ];
    protected $fillable = ['id', 'email'];
}
