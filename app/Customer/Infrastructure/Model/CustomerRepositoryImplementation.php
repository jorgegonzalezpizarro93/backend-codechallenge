<?php
/**
 * Created by PhpStorm.
 * User: jorgegonzalez
 * Date: 2/11/18
 * Time: 8:18
 */

namespace App\Customer\Infrastructure\Model;

use App\Customer\Domain\CustomerRepository;
use App\Infrastructure\AbstractRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class CustomerRepositoryImplementation extends AbstractRepository implements CustomerRepository
{
    const MODEL = '\CustomerModel';
    private const ENTITY = 'App\Customer\Domain\Customer';

    public function __construct(App $app)
    {
        parent::__construct($app);
    }

    /**
     * @param string $email
     * @return CustomerModel
     */
    public function findByEmail(string $email)
    {
        $model = $this->model->newQuery()->where('email', '=', $email)->first();
        if ($model instanceof Model) {
            return $this->toEntity($model);
        }

        return;
    }

    /**
     * @param Model $model
     * @return Entity
     */
    protected function toEntity(Model $model)
    {
        $entity = self::ENTITY;

        return $entity::create(
            $model->getKey(),
            $model->getAttribute('email')
        );

    }

    protected function modelClass()
    {

        return 'App\Customer\Infrastructure\Model'.self::MODEL;
    }
}
