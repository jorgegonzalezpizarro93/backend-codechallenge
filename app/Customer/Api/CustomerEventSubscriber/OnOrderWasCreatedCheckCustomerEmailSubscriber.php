<?php
/**
 * Created by PhpStorm.
 * User: jorgegonzalez
 * Date: 2/11/18
 * Time: 8:15
 */

namespace App\Customer\Api\CustomerEventSubscriber;

use App\Customer\Application\GetCustomerByEmailCommand;
use App\Customer\Application\GetCustomerByEmailUseCase;
use App\DomainEvent;

class OnOrderWasCreatedCheckCustomerEmailSubscriber
{
    private $getCustomerByEmailUseCase;

    public function __construct(GetCustomerByEmailUseCase $getCustomerByEmailUseCase)
    {
        $this->getCustomerByEmailUseCase = $getCustomerByEmailUseCase;
    }

    public function handle(DomainEvent $domainEvent)
    {
        $eventInformation = $domainEvent->getEvent();

        return $this->getCustomerByEmailUseCase->dispatch(
            new GetCustomerByEmailCommand(
                $eventInformation['order_id'],
                $eventInformation['email']
            )
        );
    }
}
