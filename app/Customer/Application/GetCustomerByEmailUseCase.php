<?php
/**
 * Created by PhpStorm.
 * User: jorgegonzalez
 * Date: 2/11/18
 * Time: 8:16
 */

namespace App\Customer\Application;

use App\Customer\Domain\Customer;
use App\Customer\Domain\CustomerNotFoundException;
use App\Customer\Domain\CustomerRepository;
use App\Infrastructure\UUidGenerator;
use JMS\Serializer\EventDispatcher\EventDispatcher;

class GetCustomerByEmailUseCase
{
    private $customerRepository;
    private $domainEventDispatcher;
    private $uuIdGenerator;

    public function __construct(
        CustomerRepository $customerRepository,
        EventDispatcher $domainEventDispatcher,
        UUidGenerator $uuIdGenerator
    )
    {

        $this->customerRepository = $customerRepository;
        $this->domainEventDispatcher = $domainEventDispatcher;
        $this->uuIdGenerator = $uuIdGenerator;
    }

    public function dispatch(GetCustomerByEmailCommand $getCustomerByEmailCommand)
    {
        $customer = $this->customerRepository->findByEmail($getCustomerByEmailCommand->getEmail());
        if (!$customer instanceof Customer) {
            throw new CustomerNotFoundException
            (
                "Customer not found",
                $getCustomerByEmailCommand->getEmail(),
                $getCustomerByEmailCommand->getOrderId()
            );
        }

        return $customer;
    }
}
