<?php
/**
 * Created by PhpStorm.
 * User: jorgegonzalez
 * Date: 2/11/18
 * Time: 8:16
 */

namespace App\Customer\Application;

class GetCustomerByEmailCommand
{
    private $email;
    private $orderId;

    public function __construct($orderId, $email)
    {
        $this->email = $email;
        $this->orderId = $orderId;
    }

    public function getOrderId()
    {
        return $this->orderId;
    }

    public function getEmail()
    {
        return $this->email;
    }
}
