<?php
/**
 * Created by PhpStorm.
 * User: jorgegonzalez
 * Date: 31/10/18
 * Time: 9:18
 */

namespace App\Order\Infrastructure\Model;

use App\Infrastructure\AbstractRepository;
use App\Infrastructure\Entity;
use App\Order\Domain\ValueObject\OrderAddress;
use App\Order\Domain\ValueObject\OrderCustomerLastName;
use App\Order\Domain\ValueObject\OrderCustomerName;
use App\Order\Domain\ValueObject\OrderDate;
use App\Order\Domain\ValueObject\OrderDriverId;
use App\Order\Domain\ValueObject\OrderEmail;
use App\Order\Domain\ValueObject\OrderHourSlot;
use App\Order\Domain\ValueObject\OrderId;
use App\Order\Domain\ValueObject\OrderPhoneNumber;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class OrderRepositoryImplementation extends AbstractRepository implements \App\Order\Domain\OrderRepository
{
    const MODEL = '\OrderModel';
    private const ENTITY = 'App\Order\Domain\Order';

    public function __construct(App $app)
    {
        parent::__construct($app);
    }

    public function deleteOrder(string $orderId)
    {

        $this->model->find($orderId)->delete();

    }

    protected function modelClass()
    {

        return 'App\Order\Infrastructure\Model'.self::MODEL;
    }

    protected function toEntity(Model $model): Entity
    {
        $entity = self::ENTITY;

        return $entity::createOrder(
            OrderId::fromString($model->getKey()),
            OrderCustomerName::create($model->getAttribute('customer_name')),
            OrderCustomerLastName::create($model->getAttribute('customer_last_name')),
            OrderEmail::create($model->getAttribute('email')),
            OrderPhoneNumber::create($model->getAttribute('phone_number')),
            OrderAddress::create($model->getAttribute('order_address')),
            OrderDate::create($model->getAttribute('order_date')),
            OrderHourSlot::fromString($model->getAttribute('order_hour_slot')),
            OrderDriverId::create($model->getAttribute('driver_id'))

        );

    }
}
