<?php
/**
 * Created by PhpStorm.
 * User: jorgegonzalez
 * Date: 31/10/18
 * Time: 9:22
 */

namespace App\Order\Infrastructure\Model;

use Illuminate\Database\Eloquent\Model;

class OrderModel extends Model
{
    public $timestamps = false;
    protected $table = 'orders';
    protected $primaryKey = 'order_id';
    protected $casts = [
        'order_id' => 'string',
    ];
    protected $fillable = [
        'order_id',
        'customer_name',
        'customer_last_name',
        'email',
        'phone_number',
        'order_address',
        'order_date',
        'order_hour_slot',
        'driver_id',
    ];
}
