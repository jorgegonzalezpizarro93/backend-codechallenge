<?php
/**
 * Created by PhpStorm.
 * User: jorgegonzalez
 * Date: 31/10/18
 * Time: 10:52
 */

namespace App\Order\Api;

use App\Order\Application\CreateOrderCommand;
use App\Order\Application\CreateOrderUseCase;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CreateOrderController
{
    private $createOrderUseCase;

    public function __construct(\App\Order\Application\CreateOrder\CreateOrderUseCase $createOrderUseCase)
    {
        $this->createOrderUseCase = $createOrderUseCase;

    }

    public function __invoke(Request $request)
    {
        $this->createOrderUseCase->dispatch(
            new \App\Order\Application\CreateOrder\CreateOrderCommand(
                $request->get('name'),
                $request->get('lastname'),
                $request->get('email'),
                $request->get('phoneNumber'),
                $request->get('orderAddress'),
                $request->get('orderDate'),
                $request->get('orderHourSlot')

            )
        );

        return Response::create("Created", 200, array());
    }
}
