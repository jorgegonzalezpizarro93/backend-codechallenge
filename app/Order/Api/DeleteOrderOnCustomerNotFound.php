<?php
/**
 * Created by PhpStorm.
 * User: jorgegonzalez
 * Date: 2/11/18
 * Time: 9:19
 */

namespace App\Order\Api;

use App\DomainEvent;
use App\Order\Application\DeleteOrderOnCustomerNotFound\DeleteOrderOnCustomerNotFoundCommand;
use App\Order\Application\DeleteOrderOnCustomerNotFound\DeleteOrderOnCustomerNotFoundUseCase;

class DeleteOrderOnCustomerNotFound
{
    public function __construct(DeleteOrderOnCustomerNotFoundUseCase $deleteOrderOnCustomerNotFoundUseCase)
    {
        $this->deleteOrderOnCustomerNotFoundUseCase = $deleteOrderOnCustomerNotFoundUseCase;
    }

    public function handle(DomainEvent $domainEvent)
    {

        $eventInformation = $domainEvent->getEvent();
        $this->deleteOrderOnCustomerNotFoundUseCase->dispatch(
            new DeleteOrderOnCustomerNotFoundCommand(
                $eventInformation['order_id'],
                $eventInformation['customer_email']
            )
        );

        return false;
    }
}
