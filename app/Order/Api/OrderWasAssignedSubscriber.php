<?php
/**
 * Created by PhpStorm.
 * User: JorgePc
 * Date: 01/11/2018
 * Time: 16:37
 */

namespace App\Order\Api;

use App\DomainEvent;
use App\Order\Application\UpdateDriverIdOnAssign\UpdateOrderOnDriverWasAssignedCommand;
use App\Order\Application\UpdateDriverIdOnAssign\UpdateOrderOnOrderWasAssigned;

class OrderWasAssignedSubscriber
{
    private $updateOrderOnOrderWasAssigned;

    public function __construct(UpdateOrderOnOrderWasAssigned $updateOrderOnOrderWasAssigned)
    {

        $this->updateOrderOnOrderWasAssigned = $updateOrderOnOrderWasAssigned;
    }

    public function handle(DomainEvent $domainEvent)
    {
        $eventInformation = $domainEvent->getEvent();

        $this->updateOrderOnOrderWasAssigned->dispatch(
            new UpdateOrderOnDriverWasAssignedCommand(
                $eventInformation['order_id'],
                $eventInformation['driver_id']
            )
        );
//        return false;
    }
}
