<?php
/**
 * Created by PhpStorm.
 * User: JorgePc
 * Date: 03/11/2018
 * Time: 15:02
 */

namespace App\Order\Domain\Exceptions;

use App\DomainException;

class InvalidFormatException extends DomainException
{
}
