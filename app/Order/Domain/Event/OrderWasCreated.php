<?php
/**
 * Created by PhpStorm.
 * User: jorgegonzalez
 * Date: 31/10/18
 * Time: 13:48
 */

namespace App\Order\Domain\Event;

use App\DomainEvent;

class OrderWasCreated extends DomainEvent
{
    public function __construct($id, array $event)
    {
        parent::__construct($id, $event, $this->createdAt());

    }

    protected function createdAt()
    {
        return new \DateTime('now');
    }
}
