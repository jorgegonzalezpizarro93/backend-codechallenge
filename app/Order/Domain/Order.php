<?php
/**
 * Created by PhpStorm.
 * User: jorgegonzalez
 * Date: 31/10/18
 * Time: 9:02
 */

namespace App\Order\Domain;

use App\Infrastructure\Entity;
use App\Order\Domain\Event\OrderWasCreated;
use App\Order\Domain\ValueObject\OrderAddress;
use App\Order\Domain\ValueObject\OrderCustomerLastName;
use App\Order\Domain\ValueObject\OrderCustomerName;
use App\Order\Domain\ValueObject\OrderDate;
use App\Order\Domain\ValueObject\OrderDriverId;
use App\Order\Domain\ValueObject\OrderEmail;
use App\Order\Domain\ValueObject\OrderHourSlot;
use App\Order\Domain\ValueObject\OrderId;
use App\Order\Domain\ValueObject\OrderPhoneNumber;

class Order extends Entity
{
    private $order_id;
    private $customerName;
    private $customerLastName;
    private $email;
    private $orderAddress;
    private $orderDate;
    private $orderHourSlot;
    private $phoneNumber;
    private $driverId;

    public function __construct(
        OrderId $orderId,
        OrderCustomerName $customerName,
        OrderCustomerLastName $customerLastName,
        OrderEmail $email,
        OrderPhoneNumber $phoneNumber,
        OrderAddress $orderAddress,
        OrderDate $orderDate,
        OrderHourSlot $orderHourSlot,
        OrderDriverId $orderDriverId = null
    )
    {
        $this->order_id = $orderId;
        $this->customerName = $customerName;
        $this->customerLastName = $customerLastName;
        $this->email = $email;
        $this->phoneNumber = $phoneNumber;
        $this->orderAddress = $orderAddress;
        $this->orderDate = $orderDate;
        $this->orderHourSlot = $orderHourSlot;
        $this->driverId = $orderDriverId;

        $this->domainEvents["orderWasCreated"] = new OrderWasCreated(
            $this->generateUuid(),
            $this->toModel()
        );

    }

    public function toModel()
    {
        return [
            'order_id' => $this->order_id->toString(),
            'customer_name' => $this->customerName->toString(),
            'customer_last_name' => $this->customerLastName->toString(),
            'email' => $this->email->toString(),
            'phone_number' => $this->phoneNumber->toString(),
            'order_address' => $this->orderAddress->toString(),
            'order_date' => $this->orderDate->toString(),
            'order_hour_slot' => $this->orderHourSlot->toString(),
            'driver_id' => $this->driverId->toString(),
        ];
    }

    public static function createOrder(
        OrderId $orderId,
        OrderCustomerName $customerName,
        OrderCustomerLastName $customerLastName,
        OrderEmail $email,
        OrderPhoneNumber $phoneNumber,
        OrderAddress $orderAddress,
        OrderDate $orderDate,
        OrderHourSlot $orderHourSlot,
        OrderDriverId $orderDriverId = null
    )
    {

        return new self(
            $orderId,
            $customerName,
            $customerLastName,
            $email,
            $phoneNumber,
            $orderAddress,
            $orderDate,
            $orderHourSlot,
            $orderDriverId
        );

    }

    /**
     * @return mixed
     */
    public function getOrderId()
    {
        return $this->order_id;
    }

    /**
     * @return OrderCustomerName
     */
    public function getCustomerName(): OrderCustomerName
    {
        return $this->customerName;
    }

    /**
     * @return OrderCustomerLastName
     */
    public function getCustomerLastName(): OrderCustomerLastName
    {
        return $this->customerLastName;
    }

    /**
     * @return OrderEmail
     */
    public function getEmail(): OrderEmail
    {
        return $this->email;
    }

    /**
     * @return OrderAddress
     */
    public function getOrderAddress(): OrderAddress
    {
        return $this->orderAddress;
    }

    /**
     * @return OrderDate
     */
    public function getOrderDate(): OrderDate
    {
        return $this->orderDate;
    }

    /**
     * @return OrderHourSlot
     */
    public function getOrderHourSlot(): OrderHourSlot
    {
        return $this->orderHourSlot;
    }

    /**
     * @return OrderPhoneNumber
     */
    public function getPhoneNumber(): OrderPhoneNumber
    {
        return $this->phoneNumber;
    }

    /**
     * @return OrderDriverId
     */
    public function getDriverId(): OrderDriverId
    {
        return $this->driverId;
    }

    public function updateDriver(OrderDriverId $driverId)
    {
        $this->driverId = $driverId;
    }
}
