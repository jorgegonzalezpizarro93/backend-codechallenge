<?php
/**
 * Created by PhpStorm.
 * User: jorgegonzalez
 * Date: 31/10/18
 * Time: 11:11
 */

namespace App\Order\Domain;

use App\Infrastructure\Repository;

interface OrderRepository extends Repository
{
    /**
     * @param string $id
     * @return Order|null
     */
    public function findById(string $id);

    public function deleteOrder(string $orderId);
}
