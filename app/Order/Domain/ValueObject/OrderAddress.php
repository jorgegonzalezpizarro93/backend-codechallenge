<?php
/**
 * Created by PhpStorm.
 * User: jorgegonzalez
 * Date: 2/11/18
 * Time: 14:39
 */

namespace App\Order\Domain\ValueObject;

use App\Exceptions\DomainExceptions\InvalidAddressFormatException;
use App\ValueObject;

class OrderAddress extends ValueObject
{
    protected function __construct($value)
    {
        parent::__construct($value);
    }

    public static function fromString(string $value)
    {
        // TODO: Implement fromString() method.
    }

    public static function create($value)
    {
        return new self($value);
    }

    public function toString()
    {
        return (string)$this->value;
    }

    protected function guard($value)
    {

        if (!is_array($value)) {
            return $value;
        }

        throw new InvalidAddressFormatException("Solo puede existir una direccion");
    }
}
