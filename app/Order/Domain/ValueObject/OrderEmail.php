<?php
/**
 * Created by PhpStorm.
 * User: JorgePc
 * Date: 03/11/2018
 * Time: 15:27
 */

namespace App\Order\Domain\ValueObject;

use App\Order\Domain\Exceptions\InvalidFormatException;
use App\ValueObject;

class OrderEmail extends ValueObject
{
    public static function create($value)
    {
        return new self($value);
    }

    public static function fromString(string $value)
    {
        // TODO: Implement fromString() method.
    }

    public function toString()
    {
        return $this->value;
    }

    protected function guard($value)
    {
        if (filter_var($value, FILTER_VALIDATE_EMAIL)) {
            return $value;
        }
        throw new  InvalidFormatException('El valor de '.self::class.' : '.$value.' debe ser válido');
    }
}
