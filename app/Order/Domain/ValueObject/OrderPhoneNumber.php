<?php
/**
 * Created by PhpStorm.
 * User: JorgePc
 * Date: 03/11/2018
 * Time: 15:03
 */

namespace App\Order\Domain\ValueObject;

use App\Order\Domain\Exceptions\InvalidFormatException;
use App\ValueObject;

class OrderPhoneNumber extends ValueObject
{
    public static function fromString(string $value)
    {
        return new self((int)$value);
    }

    public static function create($value)
    {
        return new self($value);
    }

    public function toString()
    {
        return $this->value;
    }

    protected function guard($value)
    {
        if (is_int($value)) {
            return $value;
        }
        throw new  InvalidFormatException('El valor de '.self::class.' : '.$value.' debe ser válido');
    }
}
