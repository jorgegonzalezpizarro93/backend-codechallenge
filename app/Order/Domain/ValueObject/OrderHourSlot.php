<?php
/**
 * Created by PhpStorm.
 * User: JorgePc
 * Date: 03/11/2018
 * Time: 15:13
 */

namespace App\Order\Domain\ValueObject;

use App\Exceptions\DomainExceptions\InvalidAddressFormatException;
use App\Order\Domain\Exceptions\InvalidFormatException;
use App\ValueObject;

class OrderHourSlot extends ValueObject
{
    public static function create($value)
    {
        return new self($value);
    }

    public static function fromString(string $value)
    {
        if (!is_array($value)) {
            return new self($value);
        }

        return new self(explode(" ", $value));
    }

    public function toString()
    {
        if (is_array($this->value)) {
            return implode(" ", $this->value);
        }
        if ($this->value instanceof \DateTime) {
            return $this->value->format('h:m');
        }

        return $this->value;
    }

    protected function guard($value)
    {

        if (!is_array($value)) {

            return $value;
        }

        if (count($value) > 1) {
            $hours = array();
            $hours["initial"] = $value[0];
            $hours["finish"] = $value[1];


            return     $this->validateHourSlot($hours);

        }

        return $value;
    }
    private function  validateHourSlot(array $hours) : array
    {
        if(!empty($hours["initial"])  && !empty($hours["finish"]))
        {
            $hourSlot=(int) $hours["finish"] - (int) $hours["initial"];
            if($hourSlot>8 || $hourSlot<0)
            {
                throw new InvalidFormatException("La franja de hora seleccionada no es válida : Hora inicial : " . $hours["initial"] . " hora final " . $hours["finish"]);

            }
            return $hours;
        }
        throw new InvalidFormatException("La franja de hora seleccionada no es válida : Hora inicial : " . $hours["initial"] . " hora final " . $hours["finish"]);

    }
}
