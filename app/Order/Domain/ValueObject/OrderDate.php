<?php
/**
 * Created by PhpStorm.
 * User: JorgePc
 * Date: 03/11/2018
 * Time: 14:55
 */

namespace App\Order\Domain\ValueObject;

use App\Order\Domain\Exceptions\InvalidFormatException;
use App\ValueObject;

class OrderDate extends ValueObject
{
    public static function create($value)
    {
        return new self($value);
    }

    public static function fromString(string $value)
    {
        // TODO: Implement fromString() method.
    }

    public function toString()
    {
        if ($this->value instanceof \DateTime) {
            return $this->value->format('d-m-y');
        }
    }

    public function toDataBaseType()
    {
        return $this->value;
    }

    protected function guard($value)
    {
        $date = \DateTime::createFromFormat('d-m-y', $value);
        if ($date instanceof \DateTime) {
            return $date;
        }
        throw new InvalidFormatException('La fecha de entrega : '.$value.' debe contener un valor válido d-m-y ');
    }
}
