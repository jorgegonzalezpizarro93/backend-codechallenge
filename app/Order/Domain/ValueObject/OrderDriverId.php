<?php
/**
 * Created by PhpStorm.
 * User: JorgePc
 * Date: 03/11/2018
 * Time: 15:32
 */

namespace App\Order\Domain\ValueObject;

use App\Order\Domain\Exceptions\InvalidFormatException;
use App\ValueObject;
use Ramsey\Uuid\Uuid;

class OrderDriverId extends ValueObject
{
    public static function create($value)
    {
        return new self($value);
    }

    public static function fromString(string $value)
    {
        // TODO: Implement fromString() method.
    }

    public function toString()
    {
        return $this->value;
    }

    protected function guard($value)
    {
        if (empty($value)) {
            return $value;
        }

        if (Uuid::isValid($value)) {
            return $value;
        }
        throw new  InvalidFormatException('El valor de '.self::class.' : '.$value.' debe ser válido');

    }
}
