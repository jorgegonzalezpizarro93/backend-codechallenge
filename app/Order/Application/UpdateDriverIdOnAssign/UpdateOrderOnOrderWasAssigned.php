<?php
/**
 * Created by PhpStorm.
 * User: JorgePc
 * Date: 01/11/2018
 * Time: 16:46
 */

namespace App\Order\Application\UpdateDriverIdOnAssign;

use App\Order\Domain\OrderRepository;
use App\Order\Domain\ValueObject\OrderDriverId;

class UpdateOrderOnOrderWasAssigned
{
    public function __construct(OrderRepository $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    public function dispatch(UpdateOrderOnDriverWasAssignedCommand $updateOrderOnDriverWasAssignedCommand)
    {

        $order = $this->orderRepository->findById($updateOrderOnDriverWasAssignedCommand->getOrderId());

        $order->updateDriver(OrderDriverId::create($updateOrderOnDriverWasAssignedCommand->getDriverId()));

        $this->orderRepository->update($order);
    }
}
