<?php
/**
 * Created by PhpStorm.
 * User: JorgePc
 * Date: 01/11/2018
 * Time: 16:45
 */

namespace App\Order\Application\UpdateDriverIdOnAssign;

class UpdateOrderOnDriverWasAssignedCommand
{
    private $orderId;
    private $driverId;

    public function __construct($orderId, $driverId)
    {

        $this->orderId = $orderId;
        $this->driverId = $driverId;
    }

    /**
     * @return mixed
     */
    public function getDriverId()
    {
        return $this->driverId;
    }

    /**
     * @return mixed
     */
    public function getOrderId()
    {
        return $this->orderId;
    }
}
