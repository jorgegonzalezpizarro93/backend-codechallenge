<?php
/**
 * Created by PhpStorm.
 * User: jorgegonzalez
 * Date: 2/11/18
 * Time: 9:21
 */

namespace App\Order\Application\DeleteOrderOnCustomerNotFound;

use App\DomainEventDispatcher;
use App\Infrastructure\UUidGenerator;

class DeleteOrderOnCustomerNotFoundUseCase
{
    public function __construct(
        \App\Order\Domain\OrderRepository $orderRepository,
        UUidGenerator $uidGenerator,
        DomainEventDispatcher $orderDispatcher
    )
    {

        $this->orderRepository = $orderRepository;
        $this->uidGenerator = $uidGenerator;
        $this->domainEventDispatcher = $orderDispatcher;
    }

    public function dispatch(DeleteOrderOnCustomerNotFoundCommand $deleteOrderOnCustomerNotFoundCommand)
    {

        $this->orderRepository->deleteOrder($deleteOrderOnCustomerNotFoundCommand->getOrderId());

    }
}
