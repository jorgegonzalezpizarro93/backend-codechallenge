<?php
/**
 * Created by PhpStorm.
 * User: jorgegonzalez
 * Date: 2/11/18
 * Time: 9:22
 */

namespace App\Order\Application\DeleteOrderOnCustomerNotFound;

class DeleteOrderOnCustomerNotFoundCommand
{
    public function __construct($orderid, $email)
    {
        $this->orderId = $orderid;
        $this->email = $email;

    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return mixed
     */
    public function getOrderId()
    {
        return $this->orderId;
    }
}
