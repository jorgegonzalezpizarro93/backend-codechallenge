<?php
/**
 * Created by PhpStorm.
 * User: jorgegonzalez
 * Date: 31/10/18
 * Time: 10:57
 */

namespace App\Order\Application\CreateOrder;

use App\DomainEventDispatcher;
use App\Infrastructure\UUidGenerator;
use App\Order\Domain\Order;
use App\Order\Domain\ValueObject\OrderAddress;
use App\Order\Domain\ValueObject\OrderCustomerLastName;
use App\Order\Domain\ValueObject\OrderCustomerName;
use App\Order\Domain\ValueObject\OrderDate;
use App\Order\Domain\ValueObject\OrderDriverId;
use App\Order\Domain\ValueObject\OrderEmail;
use App\Order\Domain\ValueObject\OrderHourSlot;
use App\Order\Domain\ValueObject\OrderId;
use App\Order\Domain\ValueObject\OrderPhoneNumber;

class CreateOrderUseCase
{
    private $orderRepository;
    private $uidGenerator;
    private $domainEventDispatcher;

    public function __construct(
        \App\Order\Domain\OrderRepository $orderRepository,
        UUidGenerator $uidGenerator,
        DomainEventDispatcher $orderDispatcher
    )
    {

        $this->orderRepository = $orderRepository;
        $this->uidGenerator = $uidGenerator;
        $this->domainEventDispatcher = $orderDispatcher;
    }

    public function dispatch(CreateOrderCommand $createOrderCommand)
    {
        $order = Order::createOrder(
            OrderId::create($this->uidGenerator::generateUuid()),
            OrderCustomerName::create($createOrderCommand->getName()),
            OrderCustomerLastName::create($createOrderCommand->getLastName()),
            OrderEmail::create($createOrderCommand->getEmail()),
            OrderPhoneNumber::create($createOrderCommand->getPhoneNumber()),
            OrderAddress::create($createOrderCommand->getOrderAddress()),
            OrderDate::create($createOrderCommand->getOrderDate()),
            OrderHourSlot::create($createOrderCommand->getOrderHourSlot()),
            OrderDriverId::create(null)
        );
        $this->orderRepository->save($order);

        foreach ($order->getDomainEvents() as $event) {

            $this->domainEventDispatcher->dispatch($event);
        }

    }
}
