<?php
/**
 * Created by PhpStorm.
 * User: jorgegonzalez
 * Date: 31/10/18
 * Time: 10:58
 */

namespace App\Order\Application\CreateOrder;

class CreateOrderCommand
{
    private $name;
    private $lastName;
    private $email;
    private $orderAddress;
    private $orderDate;
    private $orderHourSlot;
    private $phoneNumber;

    public function __construct($name, $lastName, $email, $phoneNumber, $orderAddress, $orderDate, $orderHourSlot)
    {
        $this->name = $name;
        $this->lastName = $lastName;
        $this->email = $email;
        $this->phoneNumber = $phoneNumber;
        $this->orderAddress = $orderAddress;
        $this->orderDate = $orderDate;
        $this->orderHourSlot = $orderHourSlot;

    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return mixed
     */
    public function getOrderAddress()
    {
        return $this->orderAddress;
    }

    /**
     * @return mixed
     */
    public function getOrderDate()
    {
        return $this->orderDate;
    }

    /**
     * @return mixed
     */
    public function getOrderHourSlot()
    {
        return $this->orderHourSlot;
    }

    /**
     * @return mixed
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }
}
