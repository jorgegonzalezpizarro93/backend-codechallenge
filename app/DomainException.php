<?php
/**
 * Created by PhpStorm.
 * User: Jorge Gonzalez
 * Date: 2/11/18
 * Time: 14:43
 */

namespace App;

use Throwable;

abstract class DomainException extends \Exception
{
    protected $httpCode;

    public function __construct(string $message = "", int $code = 0, Throwable $previous = null)
    {
        $this->httpCode = 400;
        parent::__construct($message, $code, $previous);
    }

    /**
     * @return int
     */
    public function getHttpCode(): int
    {
        return $this->httpCode;
    }
}
