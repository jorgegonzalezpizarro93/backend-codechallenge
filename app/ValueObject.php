<?php
/**
 * Created by PhpStorm.
 * User: Jorge Gonzalez
 * Date: 2/11/18
 * Time: 14:35
 */

namespace App;

abstract class ValueObject
{
    protected $value;

    protected function __construct($value)
    {
        $this->value = $this->guard($value);
    }

    protected abstract function guard($value);

    public abstract static function create($value);

    public abstract static function fromString(string $value);

    public abstract function toString();
}
