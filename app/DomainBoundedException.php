<?php
/**
 * Created by PhpStorm.
 * User: JorgePc
 * Date: 02/11/2018
 * Time: 19:30
 */

namespace App;

use Throwable;

class DomainBoundedException extends \Exception
{
    protected $httpCode;

    public function __construct(string $message = "", int $code = 0, Throwable $previous = null, $httpCode = null)
    {
        $this->httpCode = $httpCode;
        parent::__construct($message, $code, $previous);
    }

    /**
     * @return int
     */
    public function getHttpCode(): int
    {
        return $this->httpCode;
    }
}
