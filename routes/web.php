<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/**
 * Post Order
 * /orders
 * Body should be Json
 *
 *
 * {
 * "name" : "jorge",
 * "email": "jorge.j.gonzalez.93@gmail.com",
 * "lastname":"gonzalez",
 * "phoneNumber": 617379364,
 * "orderAddress":"c/ Fernando Poo 11 6ºB",
 * "orderDate" : "11-01-12",
 * "orderHourSlot":
 * [
 * "16:00",
 * "17:00"
 * ]
 *
 * }
 *
 */
Route::post('/orders', 'App\Order\Api\CreateOrderController');
/**
 * Get drivers filter by order date
 * /drivers?driverId=$DRIVER_ID?orderDate=$DATE
 *
 *
 */
Route::get('/dailytasks', 'App\Driver\Api\DailyTasksDriverController');
